package controllers;

import models.Todo;
import play.data.*;
import views.html.index;
import play.mvc.*;


import javax.inject.Inject;



public class HomeController extends Controller {

    private final FormFactory formFactory;

    @Inject
    public HomeController(FormFactory formFactory){
        this.formFactory = formFactory;
    }



    public Result index() {
        Form<Todo> form = formFactory.form(Todo.class);
        return Results.ok(index.render(Todo.find.all()));
    }


    public Result create() {
        Form<Todo> form;
        form = formFactory.form(Todo.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest();
        } else {
            Todo todo = form.get();
            todo.save();
            return index();
        }
    }


    public Result show() {
        return Results.ok();
    }

}
