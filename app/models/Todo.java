package models;

import io.ebean.*;

import javax.persistence.*;

    @Entity
    public class Todo extends Model {


        public static final Finder<Long, Todo> find = new Finder<>(Todo.class);

        @Id
        public Long id;

        @Column
        public String name;

        @Column
        public String time_limit;
    }
