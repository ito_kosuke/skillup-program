# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table todo (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  time_limit                    varchar(255),
  constraint pk_todo primary key (id)
);


# --- !Downs

drop table if exists todo;

